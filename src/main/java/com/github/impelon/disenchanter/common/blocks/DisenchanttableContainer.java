package com.github.impelon.disenchanter.common.blocks;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import com.github.impelon.disenchanter.common.core.DisenchanttableCore;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IWorldPosCallable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.items.CapabilityItemHandler;

import javax.annotation.Nullable;

import static com.github.impelon.disenchanter.common.core.DisenchanttableCore.Disenchanttable_Container;

public class DisenchanttableContainer extends Container {

    private TileEntity tileEntity;
    private PlayerEntity playerEntity;
    private PlayerInventory playerInventory;

    public DisenchanttableContainer(int windowId, World world, BlockPos pos, PlayerInventory playerInventory, PlayerEntity player) {
        super(Disenchanttable_Container, windowId);
        tileEntity = world.getTileEntity(pos);
        tileEntity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY);
        this.playerEntity = player;
        this.playerInventory = playerInventory;
    }

    @Override
    public boolean canInteractWith(PlayerEntity playerIn) {
        return isWithinUsableDistance(IWorldPosCallable.of(tileEntity.getWorld(), tileEntity.getPos()), playerEntity, DisenchanttableCore.DisenchanttableBlock);
    }


}
