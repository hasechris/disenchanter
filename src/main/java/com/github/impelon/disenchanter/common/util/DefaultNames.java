package com.github.impelon.disenchanter.common.util;

import com.github.impelon.disenchanter.common.core.DisenchanttableReference;

public class DefaultNames {
    public static final String Default_Disenchanttable = DisenchanttableReference.MODID + ":disenchanttable";
    public static final String Default_Disenchanttable_automatic = DisenchanttableReference.MODID + ":disenchanttable_automatic";
    public static final String Default_Disenchanttable_bulk = DisenchanttableReference.MODID + ":disenchanttable_bulk";
    public static final String Default_Disenchanttable_voiding = DisenchanttableReference.MODID + ":disenchanttable_voiding";

    // BlockNamen
    public static final String BLOCK_DISENCHANTTABLE = Default_Disenchanttable;
    public static final String BLOCK_DISENCHANTTABLE_AUTOMATIC = Default_Disenchanttable_automatic;
    public static final String BLOCK_DISENCHANTTABLE_BULK = Default_Disenchanttable_bulk;
    public static final String BLOCK_DISENCHANTTABLE_VOIDING = Default_Disenchanttable_voiding;

    //Items
    public static final String ITEM_DISENCHANTTABLE = DisenchanttableReference.MODID + ":item_disenchanttable";
    public static final String ITEM_DISENCHANTTABLE_AUTOMATIC = DisenchanttableReference.MODID + ":item_disenchanttable";
    public static final String ITEM_DISENCHANTTABLE_BULK = DisenchanttableReference.MODID + ":item_disenchanttable_bulk";
    public static final String ITEM_DISENCHANTTABLE_VOIDING = DisenchanttableReference.MODID + ":item_disenchanttable_voiding";

    // TileEntitys
    public static final String TileEntity_DISENCHANTTABLE = Default_Disenchanttable;

    //Containers
    public static final String Container_DISENCHANTTABLE = Default_Disenchanttable;
}
