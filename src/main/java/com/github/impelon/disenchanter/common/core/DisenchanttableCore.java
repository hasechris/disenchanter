package com.github.impelon.disenchanter.common.core;

import com.github.impelon.disenchanter.common.DisenchanterMain;
import com.github.impelon.disenchanter.common.blocks.DisenchanttableBlock;
import com.github.impelon.disenchanter.common.blocks.DisenchanttableContainer;
import com.github.impelon.disenchanter.common.items.DisenchanttableItem;
import com.github.impelon.disenchanter.common.util.DefaultNames;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.item.Item;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.extensions.IForgeContainerType;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.ObjectHolder;

public class DisenchanttableCore {
    public static Item.Properties itemBuilder;
    @ObjectHolder(DefaultNames.BLOCK_DISENCHANTTABLE)
    public static DisenchanttableBlock DisenchanttableBlock;

    @ObjectHolder(DefaultNames.ITEM_DISENCHANTTABLE)
    public static Item DisenchanttableItem;

    @ObjectHolder(DefaultNames.Container_DISENCHANTTABLE)
    public static ContainerType<DisenchanttableContainer> Disenchanttable_Container;

    public DisenchanttableCore() {

    }

    @Mod.EventBusSubscriber(modid = DisenchanttableReference.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
    public static class Registration{

        @SubscribeEvent
        public static void registerBlocks(final RegistryEvent.Register<Block> event) {

            IForgeRegistry<Block> blockRegistry = event.getRegistry();

            blockRegistry.register(new DisenchanttableBlock(Block.Properties.create(Material.ANVIL).hardnessAndResistance(3.0F,3.0F).lightValue(15)));

        }

        @SubscribeEvent
        public static void registerItems(final RegistryEvent.Register<Item> event) {
            IForgeRegistry<Item> itemRegistry = event.getRegistry();

            itemBuilder = (new Item.Properties()).group(DisenchanttableGroups.DISENCHANTTABLE);

            itemRegistry.register(new DisenchanttableItem(DisenchanttableBlock, itemBuilder ));

        }

        @SubscribeEvent
        public static void onContainerRegistry(final RegistryEvent.Register<ContainerType<?>> event) {
            IForgeRegistry<ContainerType<?>> ContainerRegistry = event.getRegistry();
            ContainerRegistry.register(IForgeContainerType.create((windowId, inv, data) -> {
                BlockPos pos = data.readBlockPos();
                return new DisenchanttableContainer(windowId, DisenchanterMain.proxy.getClientWorld(), pos, inv, DisenchanterMain.proxy.getClientPlayer());
            }).setRegistryName("disenchanttable"));
        }
    }
}
