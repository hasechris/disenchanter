package com.github.impelon.disenchanter.client;

import com.github.impelon.disenchanter.server.ServerProxy;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.world.World;

public class ClientProxy extends ServerProxy {
    public void ClientProxy() {

    }

    @Override
    public void preInit() {
        super.preInit();
    }

    @Override
    public World getClientWorld() { return Minecraft.getInstance().world; }

    public PlayerEntity getClientPlayer() { return Minecraft.getInstance().player; }
}
